<?php
// Template Name: Pagina Inicial
?>
<?php get_header(); ?>
<main>
        <!-- SEÇÃO 2 -->
        <a name="sec2"><div class="section_serv_solucoes"></a> 
            <h1 class="titulo_serv_solucoes">SERVIÇOS E SOLUÇÕES</h1>
            <h3 class="subtitulo_serv_solucoes">Utilizamos da tecnologia e da inovação para que você se destaque no mercado</h3>
            <div class="container_cards"> 
                <div class="card_serv_solucoes">
                    <h3>Desenvolvimento Web</h3>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
                    <div class="descricao_desenvolvimento"> 
                        <p><i class="fa-solid fa-check"></i> Desenvolvimento Backend</p>
                        <p><i class="fa-solid fa-check"></i> Desenvolvimento Frontend</p>
                    </div>
                    <button class="btn_faleconosco">FALE CONOSCO <i class="fa-brands fa-whatsapp"></i></button>
                </div>
                <div class="card_serv_solucoes">
                    <h3>Programação Wordpress</h3>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
                    <div class="descricao_desenvolvimento"> 
                        <p><i class="fa-solid fa-check"></i> Desenvolvimento do Template</p>
                        <p><i class="fa-solid fa-check"></i> Desenvolvimento de Plugin</p>
                    </div>
                    <button class="btn_faleconosco">FALE CONOSCO <i class="fa-brands fa-whatsapp"></i></button>
                </div>
                <div class="card_serv_solucoes">
                    <h3>Aplicação Mobile</h3>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
                    <div class="descricao_desenvolvimento"> 
                        <p><i class="fa-solid fa-check"></i> Desenvolvimento Android</p>
                        <p><i class="fa-solid fa-check"></i> Desenvolvimento iOS</p>
                    </div>
                    <button class="btn_faleconosco">FALE CONOSCO <i class="fa-brands fa-whatsapp"></i></button>
                </div>
            </div>
        </div>

        <!--SEÇÃO 3 -->
        <div class="section_slide">
            <h1 class="titulo_slide">NOSSOS PORTIFÓLIOS</h1>
            
        <div class="slideshow-container">
        <!-- Full-width images with number and caption text -->
            <div class="mySlides fade">
              <div class="cards">
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
              </div>
            </div>

            <div class="mySlides fade">
              <div class="cards">
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
              </div>
            </div>
          
            <div class="mySlides fade">
              <div class="cards">
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/base.png"></img>
              </div>
            </div>
            <!-- Next and previous buttons -->
            <!-- <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>  -->
             <!-- The dots/circles -->
            <div class="box_dot"style="text-align:center">
                <span class="dot" onclick="currentSlide(1)"></span>
                <span class="dot" onclick="currentSlide(2)"></span>
                <span class="dot" onclick="currentSlide(3)"></span>
              </div>
          </div>
        </div>  
        </div>
        
        <!-- SEÇÃO 4 -->
        <a name="sec4"><div class="section_resultadosmetas"></a>
            <h1 class="titulo_resultadosmetas">RESULTADOS E METAS</h1>
            <h3 class="subtitulo_resultadosmetas">A fim de que, você tenha confiança e segurança em nosso trabalho, fornecemos
                nossos dados com transparência. Para que possamos fazer parte da sua história.</h3>
            <div class="box_resultadosmetas">
            <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/graf.png"></img>
                <h1>90% DE SATISFAÇÃO DOS CLIENTES</h1>
            </div>
            <div class="box_resultadosmetas" id="2">
                <h1>+43 PROJETOS CONCLUÍDOS</h1>
                <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/graf.png"></img>
            </div>
            <div class="box_resultadosmetas" id="3">
            <img src="<?php echo get_stylesheet_directory_uri() ?>\assets/graf.png"></img>
                <h1>7 ANOS DE EXPERIÊNCIA NO MERCADO</h1>
            </div>
        </div>
    </main>
<?php get_footer(); ?>


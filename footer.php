<a name="sec5"><footer></a>
        <div class="container_footer">
            <div class="footer_left">
                <a href="#sec2"><p>Serviços</p></a>
                <p>Portfólios</p>
                <a href="#sec4"><p>Quem Somos</p></a>
                <p>Contato</p>
            </div>
            <div class="footer_center">
                <h1>LOGO</h1>
                <h5 class="endereco">Rua Quitandas, n°124, Ipituba<br>
                CEP 76854-065 - Carvalho - RJ</h5>
                <div class="redes_sociais">
                    <i class="fa-brands fa-instagram"></i>
                    <i class="fa-brands fa-whatsapp"></i>
                    <i class="fa-brands fa-facebook"></i>
                    <i class="fa-regular fa-envelope"></i>
                </div>
            </div>
            <div class="footer_right">
                <p>Políticas de Cookies</p>
                <p>Termos de Uso</p>
            </div>
        </div>
        <div class="box_copyright">
            <p>@Copyright | CNPJ: 12. 840. 829/0001-88</p>
        </div>
    </footer>
    <?php wp_footer(); ?> 
</body>
</html>